# Word Calculator

## Background
The word calculator is a program that converts the words typed by the user into numbers and symbols and then uses them to solve math solutions.

## Description
This program was created with C++ to convert words into numbers and symbols to solve math. 

It works by writing a sentence of a number, a symbol/modifier and another number. It then checks these words with different lists of existing words and their associate number/symbol and then converts these words. It then calculates this and prints out the result.

The program also tells you if you have entered a word wrong or have entered a invalid word.

This program can calculate numbers between 1-999 and understands the symbols for addition, subtraction, multiplication and division.

## Installation
Clone it to .git and open it with any type of IDE(integrated development environment). 
Program was created in C++ with VScode togheter with WSL(Windows subsystems for linux) and Cmake to compile the project.

## Usage
A program used to calculate numbers between 1-999.
The program works by writing a mathematical statement in words and see the result in numbers.
The following commands can be used in this program:

-plus (+) (thirty two plus twenty five)

-minus (-) (eighty five minus fifteen)

-times (*) (sixteen times eleven)

-over (/) (thirty five over five)

![PlusImage](https://gitlab.com/embedded-development1/wordcalculator/-/raw/main/images/Plus.PNG)*Plus*

![MinusImage](https://gitlab.com/embedded-development1/wordcalculator/-/raw/main/images/Minus.PNG)*Minus*

![TimesImage](https://gitlab.com/embedded-development1/wordcalculator/-/raw/main/images/Times.PNG)*Times*

![OverImage](https://gitlab.com/embedded-development1/wordcalculator/-/raw/main/images/Over.PNG)*Over*

## Support
Send email to martin.dahren@se.experis.com for help with any problem you are having.

## Contributing
You are welcome to clone the project and make small changes to the program.
You can also fork the project and add new features if you give credit to the author. 
Bigger changes should have an issue opened to discuss what changes you want to do.

## License
[MIT](https://choosealicense.com/licenses/mit/)
