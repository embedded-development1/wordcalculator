#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <map>
#include <sstream>
#include "calculator.h"
using namespace std;

//map lists. Associates words with numbers or symbols
map<string,double> digits ={
    {"one", 1},
    {"two", 2},
    {"three", 3},
    {"four", 4},
    {"five", 5},
    {"six", 6},
    {"seven", 7},
    {"eight", 8},
    {"nine", 9},
    {"ten", 10},
    {"eleven", 11},
    {"twelve", 12},
    {"thriteen", 13},
    {"fourteen", 14},
    {"fifteen", 15},
    {"sixteen", 16},
    {"seventeen", 17},
    {"eighteen", 18},
    {"ninteen", 19},
    {"twenty", 20},
    {"thirty", 30},
    {"fourty", 40},
    {"fifty", 50},
    {"sixty", 60},
    {"seventy", 70},
    {"eighty", 80},
    {"ninty", 90},
    {"hundred", 100}
};
map<string,double> splitters ={
    {"hundred", 100}
};
map<string,char> modifiers ={
    {"plus", '+'},
    {"minus", '-'},
    {"times", '*'},
    {"over", '/'},
};

//Takes a string sentence, lower case all the letters and place all the words in a vector
vector<string> ProcessWord(string input){
    transform(input.begin(), input.end(), input.begin(), ::tolower);
    string buffer;
    stringstream ss(input);

    vector<string> tokens;
    while(ss >> buffer){
        tokens.push_back(buffer);
    }
    return tokens;
}
//Checks to see if all the words in a vector exists in any of the map lists.
bool ValidWords(vector<string> tokens_){
    map<string,char>::iterator it;
    map<string,double>::iterator itCheck;
    for (size_t i = 0; i < tokens_.size(); i++)
    {
        it = modifiers.find(tokens_[i]);
        if(it != modifiers.end()){
            continue;
        }
        itCheck = digits.find(tokens_[i]);
        if(itCheck != digits.end()){
            continue;
        }
        cout<<"Word does not exist in list! Check words to see if they are spelled correctly and have proper spaces."<<endl;
        cout<<"Misspelled word: " + tokens_[i]<<endl;
        return false;
    }
    return true;
}

//Interprets the words in a vector and decide if they are a number or a symbol
double InterperetWords(vector<string> tokens){
    size_t currentnum = 0;
    vector<string> firstNumberTokens;
    map<string,char>::iterator it;
    map<string,double>::iterator itCheck;
    for (currentnum; currentnum < tokens.size(); currentnum++)
    {
        it = modifiers.find(tokens[currentnum]);
        if(it != modifiers.end()){
            break;
        }
        firstNumberTokens.emplace_back(tokens[currentnum]);
    }
    double firstNum = CreateNumber(firstNumberTokens);
    char modifier = GetModifier(tokens[currentnum]);
    currentnum++;
    vector<string> secondNumberTokens;
    for (currentnum; currentnum < tokens.size(); currentnum++)
    {
        secondNumberTokens.emplace_back(tokens[currentnum]);
    }
    double secondNum = CreateNumber(secondNumberTokens);
    double result = calculate(firstNum, secondNum, modifier);
    return result;
}

//gets the symbol/modifier from a word
char GetModifier(string modi){
    char mod = modifiers[modi];
    return mod;
}

//Creates number from a collection of words
double CreateNumber(vector<string> tokens_){
    double result = 0;
    double partialResult = 0;
    map<string,double>::iterator it;
    for (size_t i = 0; i < tokens_.size(); i++)
    {
        it = splitters.find(tokens_[i]);
        if(it != splitters.end()){
            if (partialResult == 0){
                partialResult = 1;
            } 
            partialResult *= digits[tokens_[i]];
            result += partialResult;
            partialResult = 0;
        }
        else{
            partialResult += digits[tokens_[i]];
        }
    }
    result += partialResult;
    return result;
}

//Calculates the numbers with the symbols/modifier
double calculate(double firstNum, double secondNum, char modifier){
    if(modifier == '+'){
        return firstNum + secondNum;
    }
    else if(modifier == '-'){
        return firstNum - secondNum;
    }
    else if(modifier == '*'){
        return firstNum * secondNum;
    }
    else if(modifier == '/'){
        return firstNum / secondNum;
    }
    else{
        return 0;
    }
}