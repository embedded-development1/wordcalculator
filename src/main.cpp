#include <iostream>
#include <vector>
#include <limits>
#include "calculator.h"
using namespace std;

bool running = true;
//Main loop of the program, checks if program needs to repeat or not
int main(){
    string test;
    while(running){
        test = "";
        cout << "Welcome to word calculator!"<<endl;
        cout << "Type numbers and modifier in words"<<endl;
        cout << "+ = plus"<<endl;
        cout << "- = minus"<<endl; 
        cout << "* = times"<<endl; 
        cout << "/ = over"<<endl;       
        cout<<"Write a sentence: "<<endl;
        getline(cin, test);
        vector<string> wordList = ProcessWord(test);
        if(ValidWords(wordList)){
            double final = InterperetWords(wordList);
            cout<<final<<endl;
            cout<<"Try Again?(y or n)"<<endl; 
        }
        else{
            cout<<"Try Again?(y or n)"<<endl;        
        }
        string x;
        cin >> x;
        while(x != "y" && x != "n"){
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            cout << "Please type y or n!"<<endl;
            cin >> x;
        }
        if(x == "n"){
            running = false;
        }
        else{
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
        }
    }
    return 0;
}